import numpy as np


class Network:

    def __init__(self, num_of_inputs, num_of_neurons_hidden, num_of_classes, learning_rate, random_weights, activation_functions):
        self.learning_rate = learning_rate
        self.num_of_classes = num_of_classes
        self.activation_functions = activation_functions

        self.W_list = []
        self.b_list = []
        self.random_weights = random_weights
        self.num_of_neurons_hidden = num_of_neurons_hidden
        self.W_list.append(
            np.random.uniform(random_weights[0], random_weights[1], (num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            self.W_list.append(np.random.uniform(random_weights[0], random_weights[1],
                                                 (num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.b_list.append(np.zeros(num_of_neurons_hidden[i]))
        self.W_list.append(
            np.random.uniform(random_weights[0], random_weights[1], (num_of_neurons_hidden[-1], num_of_classes)))
        self.b_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_list.append(np.zeros(num_of_classes))

        self.outputs = []
        self.impulses = []
        self.best_loss = None
        self.bestW = None
        self.bestB = None

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def sigmoid_derivatives(self, x):
        sigmoid_value = self.sigmoid(x)
        return sigmoid_value * (1 - sigmoid_value)

    def softmax_derivative(self, predicted, actual, impulse):
        softmax_result = self.softmax(impulse)
        softmax_dv_result = np.zeros(softmax_result.shape)
        for i in range(softmax_result.shape[0]):
            cos = np.reshape(softmax_result[i], newshape=(softmax_result[i].shape[0], 1))
            e = np.ones(shape=(softmax_result.shape[1], 1))
            I = np.eye(N=softmax_result.shape[1])
            dev = (cos @ e.T) * (I - e @ cos.T)
            softmax_dv_result[i] = dev @ (actual[i] - predicted[i])
        return softmax_dv_result

    def softmax(self, a):
        return np.exp(a - np.max(a)) / np.sum(np.exp(a - np.max(a)))

    def relu(self, x):
        return np.maximum(x, 0)

    def relu_derivative(self, x):
        x[x <= 0] = 0
        x[x > 0] = 1
        return x

    def forward_pass_step(self, weights, bias, inputs, activation_func):
        full_impulse = inputs @ weights + bias
        return activation_func(self, x=full_impulse), full_impulse

    def forward_pass(self, data):
        self.outputs.clear()
        self.impulses.clear()
        output, impulse = self.forward_pass_step(self.W_list[0], self.b_list[0], data, self.activation_functions[0][0])
        self.outputs.append(output)
        self.impulses.append(impulse)
        for i in range(1, len(self.W_list)):
            output, impulse = self.forward_pass_step(self.W_list[i], self.b_list[i], self.outputs[-1], self.activation_functions[i][0])
            self.outputs.append(output)
            self.impulses.append(impulse)

        return np.argmax(self.outputs[-1], axis=1)

    def back_propagation(self, data, actual_labels):
        actual_labels_one_hot = self.to_one_hot_encoding(actual_labels, self.num_of_classes)
        num = data.shape[0]

        deltas = []
        delta = self.activation_functions[-1][1](self, x=self.impulses[-1]) * (actual_labels_one_hot - self.outputs[-1])
        self.W_list[-1] += self.learning_rate * (self.outputs[-2].T @ delta / num)
        self.b_list[-1] += self.learning_rate * (delta.T @ (np.ones(num)) / num)
        deltas.append(delta)
        for i in range(1, len(self.W_list) - 1):
            delta = self.activation_functions[-1-i][1](self, x=self.impulses[-1 - i]) * (deltas[-1] @ self.W_list[-1 * i].T)
            self.W_list[-1 - i] += self.learning_rate * (self.outputs[-2 - i].T @ delta / num)
            self.b_list[-1 - i] += self.learning_rate * (delta.T @ (np.ones(num)) / num)
            deltas.append(delta)

        delta = self.activation_functions[0][1](self, x=self.impulses[0]) * (deltas[-1] @ self.W_list[1].T)
        self.W_list[0] += self.learning_rate * (data.T @ delta / num)
        self.b_list[0] += self.learning_rate * (delta.T @ (np.ones(num)) / num)
        deltas.append(delta)

    def calculate_accuracy_and_loss(self, x, y):
        predicted_labels = self.forward_pass(x)
        last_output = self.outputs[-1]
        loss = np.sum((last_output - self.to_one_hot_encoding(y, self.num_of_classes)) ** 2) / x.shape[0]
        return np.sum(predicted_labels == y) / len(y), loss

    def check_for_best_loss(self, loss):
        if (self.best_loss is not None and loss < self.best_loss) or (self.best_loss is None):
            print(f"\tSaving best weights. Best loss was {self.best_loss}, new loss is {loss}")
            self.best_loss = loss
            self.bestW = self.W_list.copy()
            self.bestB = self.b_list.copy()

    def to_one_hot_encoding(self, labels, num_of_classes):
        labels_one_hot = np.zeros(shape=(labels.shape[0], num_of_classes))
        for i in range(len(labels)):
            labels_one_hot[i][labels[i]] = 1
        return labels_one_hot
