import DataLoader
import numpy as np

from Network import Network

np.random.seed(127)


def train(model, x_t, y_t, x_v, y_v, batch_size, epochs, weights):
    header_string = "Training accuracy;Training loss;Validation accuracy;Validation loss"
    csv_lines = [header_string]
    for i in range(epochs):
        for j in range(0, len(y_t), batch_size):
            model.forward_pass(x_t[j:j + batch_size])
            model.back_propagation(x_t[j:j + batch_size], y_t[j:j + batch_size])
        training_accuracy, training_loss = model.calculate_accuracy_and_loss(x_t, y_t)
        validation_accuracy, validation_loss = model.calculate_accuracy_and_loss(x_v, y_v)
        print(
            f"Epoch no. {i} - training accuracy: {training_accuracy}, training loss: {training_loss}, validation accuracy {validation_accuracy}, validation loss {validation_loss}")
        model.check_for_best_loss(validation_loss)
        csv_lines.append(str(training_accuracy) + ";" + str(training_loss) + ";" + str(validation_accuracy) + ";" + str(
            validation_loss))
    save_to_csv(csv_lines,
                "LR=" + str(model.learning_rate) +
                ", batch_size=" + str(batch_size) +
                ", hidden_layer=" + str(model.num_of_neurons_hidden[0]) +
                ", weights=" + str(weights) +"relu"+ ".csv")


def save_to_csv(lines, name):
    with open("results/" + name, 'w') as file:
        for line in lines:
            file.write(line)
            file.write('\n')


if __name__ == '__main__':
    training, validation, testing, num_of_classes = DataLoader.load_data('data/mnist.pkl')
    num_of_inputs = training[0].shape[1]

    epochs_for_all = 25
    batch_sizes = [10, 25, 50, 100, 200]
    random_weights_list = [(-2.0 / 28.0, 2.0 / 28.0), (-1.0 / 10.0, 1.0 / 10.0), (-2.5 / 10.0, 2.5 / 10.0),
                           (-1.0 / 2.0, 1.0 / 2.0)]

    num_of_neurons_hidden_layer_list = [50, 100, 150, 200, 500]

    activation_functions = [
        [(Network.sigmoid, Network.sigmoid_derivatives),
         (Network.sigmoid, Network.sigmoid_derivatives)],
        [(Network.relu, Network.relu_derivative),
         (Network.sigmoid, Network.sigmoid_derivatives)]
    ]


    network = Network(num_of_inputs,
                      [500],
                      num_of_classes,
                      0.1,
                      (-0.1,0.1),
                      activation_functions[1])

    train(network, training[0], training[1], validation[0], validation[1], batch_size=25, epochs=500,
          weights=(-0.1,0.1))


    # Expermients for batch sizes
    network = Network(num_of_inputs,
                      [200],
                      num_of_classes,
                      0.1,
                      (-2.0 / 28.0, 2.0 / 28.0),
                      activation_functions[0])


    # Expermients for weights
    for i in range(len(random_weights_list)):
        network = Network(num_of_inputs,
                          [200],
                          num_of_classes,
                          0.1,
                          random_weights_list[i],
                          activation_functions[0])

        train(network, training[0], training[1], validation[0], validation[1], batch_size=50, epochs=epochs_for_all,
              weights=random_weights_list[i])

    # Expermients for number of neurons in hidden layer
    for i in range(len(num_of_neurons_hidden_layer_list)):
        network = Network(num_of_inputs,
                          [num_of_neurons_hidden_layer_list[i]],
                          num_of_classes,
                          0.1,
                          (-2.0 / 28.0, 2.0 / 28.0),
                          activation_functions[0])

        train(network, training[0], training[1], validation[0], validation[1], batch_size=50, epochs=epochs_for_all,
              weights=(-2.0 / 28.0, 2.0 / 28.0))

    # Expermients for activation functions
    for i in range(1,len(activation_functions)):
        network = Network(num_of_inputs,
                          [200],
                          num_of_classes,
                          0.1,
                          (-2.0 / 28.0, 2.0 / 28.0),
                          activation_functions[i])

        train(network, training[0], training[1], validation[0], validation[1], batch_size=50, epochs=epochs_for_all,
              weights=(-2.0 / 28.0, 2.0 / 28.0))
